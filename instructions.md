These instructions are just my interpretation of the instructions at [LiveEatLearn.com](https://www.liveeatlearn.com/the-simple-guide-to-kickass-kombucha/). They assume you already have a SCOBY. If not, try to follow the instructions
[here](https://www.liveeatlearn.com/the-simple-guide-to-kickass-kombucha/)

## Materials

(assumes you're making 1 gallon)

#### For First Fermentation

(NB this assumes you're making 1 gallon)

- 1 gallon glass or ceramic jar(s)
- A pot to boil 14 cups of water
- 14 cups of clean water
- 1 cup (200g) white sugar
- 8 bags of tea or 2 Tablespoons of loose leaf tea
- 2 cups of unpasteurized, unflavored starter kombucha

#### For Second Fermentation

- Enough fermenting bottles to hold 1 gallon of kombucha (this is different from
  the 1 gallon glass or ceramic jar above, and will be used in the second
  fermentation process below.
- [optional] Sweetener. Here are a few ideas per 1 cup kombucha:
  - 1 to 2 Tbsp mashed fruit or fruit juice
  - 1 slice of orange
  - 1 to 2 tsp honey
  - 1 piece of peppermint candy
  - 1 piece of candied ginger

[This website](http://yumuniverse.com/how-to-flavor-homemade-kombucha-tea/) has
some unique and tasty flavoring ideas as well

## Instructions

#### First Fermentation

(NB this assumes you're making 1 gallon)

0. Sanitize your jar(s) in boiling water. The jars should be 1/2 - 1 gallon
   glass or ceramic jars.
1. Boil 14 cups of clean water in a pot; remove from heat.
2. Dissolve 1 cup (200g) white sugar into the water.
3. Add 8 bags of tea or 2 Tablespoons of loose leaf tea to the water. Leave the
   tea in the water until the water comes down to room temperature.
4. Once the water is room temp, pour it into the sanitized jar(s) from step 1.
5. Pour 2 cups of unpasteurized, unflavored starter kombucha into the jar(s).
6. Gently place the SCOBY into the jar(s).
7. Cover the jar(s) with a couple of coffee filters each and secure them to the
   lid.
8. Place the jar(s) somewhere dark, still, and at room temp for 6 days.
9. On day 6, taste the kombucha by gently drawing some out with a straw. If it's
   the flavor you want, continue on, otherwise let it sit for another day and
   repeat this step. Remember, the longer it ferments the less sweet and more
   vinegary it'll be.
10. Set aside 2 cups, and the SCOBY, to use as starter kombucha for your next
    batch

#### Second Fermentation

1. Strain kombucha into fermenting bottles. Make sure to leave plenty of room
   (1-2 inches) in the bottles for the carbonation, otherwise the bottles might
   pop.
2. Add flavoring [optional]
3. Put the bottles somewhere dark, still, and at room temp for 3 days. Open a
   bottle and taste it. If you like it, you're finished! If you want it less
   sweet or more fizzy, repeat this step in 1 - 2 days.